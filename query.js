const mysql = require('mysql');

const mysqlQuery = {};

const con = mysql.createConnection({
  host: 'XXXX',
  user: 'XXXX',
  password: 'XXXX',
  database: 'XXXX',
});

const vHold = (arr) => {
  const result = {};
  for (let i = 0; i < arr.length; i += 1) {
    result[arr[i]] = 1;
  }
  return result;
};

const getQuery = (table, col, val) => new Promise((resolve) => {
  const values = val;
  const sql = `SELECT \`${col}\` FROM \`${table}\``;
  con.query(sql, (err, rows) => {
    if (err) throw err;
    for (let i = 0; i < rows.length; i += 1) {
      if (rows[i][col] !== 'null') {
        let prop = rows[i][col].toLowerCase();
        prop = prop.replace(/ /g, '');
        if (prop.includes(',')) {
          const sProp = prop.split(',');
          for (let j = 0; j < sProp.length; j += 1) {
            if (values[sProp[j]]) {
              values[sProp[j]] += 1;
            }
          }
        } else if (values[prop]) {
          values[prop] += 1;
        }
      }
      if (i + 1 === rows.length) {
        resolve(values);
      }
    }
  });
});

const getTitles = table => new Promise((resolve) => {
  const sql = `SELECT * FROM \`${table}\``;
  con.query(sql, (err, rows) => {
    if (err) throw err;
    resolve(rows);
  });
});


const getGen = x => new Promise((resolve) => {
  const gens = {};
  for (let i = 0; i < x.length; i += 1) {
    let a = x[i].title.toLowerCase();
    a = a.match(/\/(.*?)\//i);
    if (a !== null) {
      if (gens[a[0]]) {
        gens[a[0]] += 1;
      } else {
        gens[a[0]] = 1;
      }
    }
  }
  const gen = makeArray(gens);
  resolve(gen);
});

const makeArray = (obj) => {
  const arr = [];
  for(prop in obj){
    arr.push([prop, obj[prop]]);
  }
  return arr;
}

const getDupeTitles = table => new Promise((resolve) => {
  const sql = `SELECT \`title\`, COUNT(*) FROM \`${table}\` GROUP BY \`title\` HAVING COUNT(*) > 1 ORDER BY COUNT(*) DESC`;
  con.query(sql, (err, rows) => {
    if (err) throw err;
    console.log(rows);
    resolve(rows);
  });
});

const getFileNames = table => new Promise((resolve) => {
  const sql = `SELECT \`filename\`, COUNT(*) FROM \`${table}\` GROUP BY \`filename\` HAVING COUNT(*) > 10 ORDER BY COUNT(*) DESC`;
  con.query(sql, (err, rows) => {
    if (err) throw err;
    resolve(rows);
  });
});

const getCountries = () => new Promise((resolve) => {
  const sql = 'SELECT `countryname`, COUNT(*) FROM `polinfo` GROUP BY `countryname` HAVING COUNT(*) > 1 ORDER BY COUNT(*) DESC';
  con.query(sql, (err, rows) => {
    if (err) throw err;
    resolve(rows);
  });
});

mysqlQuery.pol = () => new Promise((resolve) => {
  getDupeTitles('poltitles')
        .then((dupes) => {
          getTitles('poltitles')
                .then((result2) => {
                  getGen(result2)
                        .then((genTitles) => {
                          getFileNames('polinfo')
                                .then((fileNames) => {
                                  getCountries()
                                      .then((cList) => {
                                        const data = {
                                          dupe: dupes,
                                          gens: genTitles,
                                          filenames: fileNames,
                                          countries: cList,
                                        };
                                        resolve(data);
                                      });
                                });
                        });
                });
        });
});

mysqlQuery.g = () => new Promise((resolve) => {
  const vals = vHold(['amd', 'intel', 'python', 'javascript', 'js', 'php', 'ruby', 'c++', 'perl', 'haskell', 'bash', 'swift', 'c', 'lisp', 'java']);
  getQuery('ginfo', 'comment', vals)
  .then((lang) => {
    getTitles('gtitles')
      .then((result2) => {
        getGen(result2)
          .then((result3) => {
            const data = {
              lan: lang,
              titles: result3,
            };
            resolve(data);
          });
      });
  });
});

mysqlQuery.fit = () => new Promise((resolve) => {
  const vals = vHold(['texasmethod', 'madcow', 'phul', 'ppl', 'ss', 'gslp', 'regpark',
    'icf', 'sl', 'startingstrength', 'greyskull', 'grayskull', 'stronglifts', 'ulpp']);
  getQuery('fitinfo', 'comment', vals)
  .then((result) => {
    getTitles('fittitles')
      .then((result2) => {
        getGen(result2)
          .then((result3) => {
            const data = {
              routine: result,
              gens: result3,
            };
            resolve(data);
          });
      });
  });
});

module.exports = mysqlQuery;

