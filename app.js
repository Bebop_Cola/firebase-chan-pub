const mysqlQuery = require('./query');
const firebase = require("firebase-admin");
const serviceAccount = require("./key.json");

firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount),
  databaseURL: "XXXX"
});
const database = firebase.database();

const updateG = () =>{
    mysqlQuery.g().then((results) => {
        database.ref('g').set({
            languages: results.lan,
            titles: results.titles,
        })
    })
}
const updateFit = () =>{
    mysqlQuery.fit().then((results) => {
        database.ref('fit').set({
            routine: results.routine,
            fitGens: results.gens,
        })
    })
}
const updatePol = () =>{
    mysqlQuery.pol().then((results) => {
        database.ref('pol').set({
            dupe: results.dupe,
            polGens: results.gens,
            fileNames: results.filenames,
            countries: results.countries,
        })
    })
}

function updateDaily(arg){
	return new CronJob('1 */6 * * *', () => {
		arg();
	}, null, true, 'America/Los_Angeles');
}
updateDaily(updateG);
updateDaily(updatePol);
updateDaily(updateFit);
